#!/usr/bin/bash
set -xe
cd $HOME

# Clone CGM
git clone https://bitbucket.org/fathomteam/cgm.git
cd cgm
autoreconf -fi

./configure \
            --prefix=$CGM_DIR \
            --with-mpi=/usr \
            --with-occ=/usr \
            --enable-static \
            --enable-optimize \
            --enable-debug \
            LDFLAGS="-L/usr/lib/x86_64-linux-gnu" \
            CFLAGS='-O2 -fPIC -DPIC' \
            CXXFLAGS='-O2 -fPIC -DPIC' \
            FCFLAGS='-O2 -fPIC' \
            FFLAGS='-O2 -fPIC'

# make check
make install
make clean

# Add PETSC_DIR to env variables of container
echo "export CGM_DIR=$CGM_DIR" >> $HOME/.bashrc
echo "export PATH=$PATH:$CGM_DIR/bin" >> $HOME/.bashrc
echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CGM_DIR/lib" >> $HOME/.bashrc

# Clean files
# cd $HOME && rm -rf moab
