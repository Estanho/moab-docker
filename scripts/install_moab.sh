#!/usr/bin/bash
set -xe
cd $HOME

# Clone MOAB
git clone https://bitbucket.org/fathomteam/moab.git
cd moab
autoreconf -fi

./configure \
            --prefix=$MOAB_DIR \
            --with-mpi=/usr \
            --with-hdf5=/usr/lib/x86_64-linux-gnu/hdf5/mpich \
            --with-netcdf=/usr \
            --with-metis=/usr \
            --enable-static \
            --enable-optimize \
            --enable-debug \
            --enable-tools \
            CFLAGS='-O2 -fPIC -DPIC' \
            CXXFLAGS='-O2 -fPIC -DPIC' \
            FCFLAGS='-O2 -fPIC' \
            FFLAGS='-O2 -fPIC'

# make check
make install
make clean

# Add PETSC_DIR to env variables of container
echo "export MOAB_DIR=$MOAB_DIR" >> $HOME/.bashrc
echo "export PATH=$PATH:$MOAB_DIR/bin" >> $HOME/.bashrc
echo "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MOAB_DIR/lib" >> $HOME/.bashrc

# Clean files
# cd $HOME && rm -rf moab
